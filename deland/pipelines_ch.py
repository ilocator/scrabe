# -*- coding: utf-8 -*-
from deland import lib
from deland import settings

def get_data(raw):
    result = []
    fields = ['bfs_no','gemeindename','canton_number','canton_code','bezirk_number','bezirk_name','planning_region','grossregionen','language']
    number_fields = ['bfs_no','canton_number','bezirk_number','planning_region','grossregionen','language']
    for i in range(len(raw)):
        if i > settings.EXCEL_SPACES['ch']:
            row = raw[i]
            obj = {}
            for i in range(len(fields)):
                field_name = 'e_%s' % fields[i]
                if fields[i] in number_fields:
                    obj[field_name] = row[i].split('.')[0]
                else:
                    obj[field_name] = row[i]
            result.append(obj)
    return result
    
class DelandPipeline(object):
    def open_spider(self, spider):
        excel = lib.get_excel(settings.EXCEL_CH)
        municipalities = get_data(excel[2])
        
        self.data = {'items':municipalities,'e_bfs_no':[],'w_sfos':[]}
        spider.pipeline = self
        
    def process_item(self, item, spider):
        self.data['w_sfos'].append(item['w_sfos'])
        return item

    def close_spider(self, spider):
        print([x for x in self.data['e_bfs_no'] if x not in self.data['w_sfos']])
        pass