# -*- coding: utf-8 -*-
from deland import lib
from deland import settings

def get_data(raw):
    result = {}
    for i in range(len(raw)):
        if i > settings.EXCEL_SPACES['nl']:
            row = raw[i]
            result[row[4]] = {'e_nut_3_code':row[0],'e_lau2_nat_code':row[2]}
    return result

class DelandPipeline(object):
    def open_spider(self, spider):
        excel = lib.get_excel(settings.EXCEL_NL)[0]
        data = get_data(excel)
        self.data = {'items':data,'nut_3':{}}
        spider.pipeline = self
        
    def process_item(self, item, spider):
        return item

    def close_spider(self, spider):
        pass