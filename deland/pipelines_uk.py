# -*- coding: utf-8 -*-
from deland import lib
from deland import settings

def get_data(raw):
    result = {}
    keys = ['name','url','website','population','area','type','county','region','country']
    for i in range(len(raw)):
        if i>settings.EXCEL_SPACES['uk']:
            row = raw[i]
            obj = {}
            for j in range(len(keys)):
                field_name = 'e_%s' % keys[j]
                if keys[j] in ['population','area']:
                    obj[field_name] = row[j].split('.')[0]
                else:    
                    obj[field_name] = row[j]
            result[row[0]] = obj
    return result

class DelandPipeline(object):
    def open_spider(self, spider):
        excel = lib.get_excel(settings.EXCEL_UK)[0]
        data = get_data(excel)
        spider.pipeline = self
        self.data = {'items':data}
        
    def process_item(self, item, spider):
        return item

    def close_spider(self, spider):
        pass