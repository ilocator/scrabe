# -*- coding: utf-8 -*-
import xlrd
from deland import settings,lib

def get_lau(raw):
    result = {}
    national_code = []
    for i in range(len(raw)):
        if i>settings.EXCEL_SPACES['gemeinden']:
            row = raw[i]
            if row[3]!='' and row[9]=='':
                obj = {}
                obj['e_land'] = row[0]
                obj['e_rb'] = row[1]
                obj['e_kreis'] = row[2]
                obj['e_vb'] = row[3]
                obj['e_gemeindename'] = row[5]
                obj['e_key'] = ' '.join([obj['e_%s' % f] for f in ['land','rb','kreis','vb']])
                if national_code:
                    result[lau_key]['e_national_code'] = ','.join(national_code)
                    national_code = []
                lau_key = i+1
                result[lau_key] = obj
            if row[9]!='':
                national_code.append(' '.join([row[x] for x in [0,1,2,4]]))
    result[lau_key]['e_national_code'] = ','.join(national_code)
    return result                

class DelandPipeline(object):
    def open_spider(self, spider):
        spider.pipeline = self
        excel = lib.get_excel()
        lau = get_lau(excel[0])
        self.data = {'items':lau,'input':[],'output':[]}
        
    def process_item(self, item, spider):
        return item

    def close_spider(self, spider):
        lib.write_list_dif('wiki_missed_lau.txt',self.data['input'],self.data['output'])