import xlrd
from deland import settings

def get_excel(path=settings.EXCEL_FILE):
    wb = xlrd.open_workbook(path)
    return [[[str(value).strip() for value in sheet.row_values(i)] for i in range(sheet.nrows)] for sheet in wb.sheets()]
    
def write_list_dif(file,list1,list2):
    open(file,'w').write('')
    for value in sorted(list(set(list1)-set(list2))):
            open(file,'a').write(str(value)+'\n')