# -*- coding: utf-8 -*-
import scrapy
import xlrd
from deland.items import DelandItem
from deland import settings

class MainSpider(scrapy.Spider):
    name = "main"
    allowed_domains = ["de.wikipedia.org"]
    start_urls = ['http://de.wikipedia.org/']
    custom_settings = {'ITEM_PIPELINES': {'deland.pipelines.DelandPipeline': 400}}

    def parse(self, response):  
        items = self.pipeline.data['items']
        i = 0 
        for key in items:
            #if key!=9613: continue
            item = items[key]
            i+=1
            title = item['e_gemeindename']
            plz = item['e_plz'].split('.')[0]
            for e_gemeindename,e_plz,e_national_code in exceptions:
                if e_gemeindename==title and plz==e_plz: 
                    plz=e_national_code
            title = title.split(', St')[0]
            title = title.split(', Stadt')[0]
            title = title.split(', Kurort')[0]
            title = title.split(', GKSt')[0]
            
            link = 'https://de.wikipedia.org/w/index.php?fulltext=1&search=%s %s' % (title,plz)
            self.pipeline.data['input'].append(item['i'])
            yield scrapy.Request(link,callback=self.parse_search,meta={'i':item['i']})

    def parse_search(self, response):
        links = response.xpath("//ul[contains(@class,'mw-search-results')]/li/div/a/@href").extract()
        if links:
            relative_url = links[0]
            link = response.urljoin(relative_url)
            yield scrapy.Request(link,callback=self.parse_land,dont_filter=True,meta={'links':links[1:],'i':response.meta.get('i')})
    
    def parse_land(self, response):
        if response.xpath("//th[text()='Basisdaten']") and response.xpath("//th[text()='Wappen']") and not response.xpath("//h1[starts-with(text(),'Landkreis')]") and response.xpath("//a[text()='Gemeindeschlüssel']"): 
            i = response.meta.get('i')
            item = DelandItem()
            item['w_title'] = response.xpath('//h1/text()').extract_first()
            item['w_url'] = response.request.url
            item['w_image_url'] = response.xpath("//th[text()='Wappen']/parent::tr/following-sibling::tr/td[1]/a/img/@src").extract_first()
            item['w_state'] = response.xpath("//a[text()='Bundesland']/parent::td/following-sibling::td/a/text()").extract_first()
            item['w_height'] = (response.xpath("//a[text()='Höhe']/parent::td/following-sibling::td/text()").extract_first() or '').split("\u00a0")[0]
            item['w_area'] = (response.xpath("//a[text()='Fläche']/parent::td/following-sibling::td/text()").extract_first() or '').split("\u00a0")[0].replace('.',',')
            item['w_inhabitants'] = response.xpath("//td[text()='Einwohner:']/following-sibling::td/text()").extract_first()
            item['w_postal_codes'] = response.xpath("//a[starts-with(text(),'Postleitzahl')]/parent::td/following-sibling::td/text()").extract_first()
            item['w_area_code'] = response.xpath("//a[text()='Vorwahl' or text()='Vorwahlen']/parent::td/following-sibling::td/text()").extract_first()
            item['w_license_number'] = response.xpath("//a[text()='Kfz-Kennzeichen']/parent::td/following-sibling::td/text()").extract_first()
            item['w_national_code'] = response.xpath("//a[text()='Gemeindeschlüssel']/parent::td/following-sibling::td/text()").extract_first()
            item['w_city_breakdown'] = ' '.join(response.xpath("//td[text()='Stadtgliederung:']/following-sibling::td/text()").extract()).replace('\n','')
            item['w_administration_address'] = ' '.join(response.xpath("//td[contains(text(),'Adresse der')]/following-sibling::td/text()").extract()).replace('\n','')
            item['w_website'] = response.xpath("//td[text()='Webpräsenz:']/following-sibling::td/a/text()").extract_first()
            item['w_major'] = (response.xpath("//a[starts-with(text(),'Oberbürgermeister')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or
                response.xpath("//a[starts-with(text(),'Bürgermeister')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or ''
                ).split('(')[0].strip()
            for key in self.pipeline.data['items'][i]:
                if key!='i':
                    item[key] = self.pipeline.data['items'][i][key]
            self.pipeline.data['output'].append(i)
            yield item
        else:
            links = response.meta.get('links')
            if links:
                relative_url = links[0]
                link = response.urljoin(relative_url)
                yield scrapy.Request(link,callback=self.parse_land,dont_filter=True,meta={'links':links[1:],'i':response.meta.get('i')})
                
exceptions = [
    ['Ransbach-Baumbach, Stadt','56235','07 1 43 062'],['Bad Ems, Stadt','56130','07 1 41 006'],
    ['Simmern/ Hunsrück, Stadt','55469','07 1 40 144'],['Kirchberg (Hunsrück), Stadt','55481','07 1 40 067'],
    ['Asbach','53567','07 1 38 003'],['Ulmen, Stadt','56766','07 1 35 083'],
    ['Rüdesheim','55593','07 1 33 117'],['Altenkirchen (Westerwald), Stadt','57610','07 1 32 501'],
    ['Limburg a.d. Lahn, Kreisstadt','65549','06 5 33 009'],['Münster, Stadt','48143','05 5 15 000'],
    ['Euskirchen, Stadt','53879','05 3 66 016'],['Wesel, Stadt','46483','05 1 70 048'],
    ['Viersen, Stadt','41747','05 1 66 032'],['Wolfenbüttel, Stadt','38300','03 1 58 037'],
    ['Leezen','23816','01 0 60 053'],['Mittelangeln','24986','01 0 59 185'],
    ['Rendsburg, Stadt','24768','01 0 58 135'],['Plön, Stadt','24306','01 0 57 057'],
    ['Viöl','25884','01 0 54 144'],['Veitsbronn','90587','09 5 73 130'],
    ['Straubing','94315','09 2 63 000'],['Mühldorf am Inn','84453','09 1 83 128'],
    ['Schondorf am Ammersee','86938','09 1 81 139'],['Sigmaringen, Stadt','72488','Thomas Schärer'],
    ['Rosenheim','83022','09 1 63 000 83022'],['Heidesheim am Rhein','55262','07 3 39 027'],
    ['Germersheim, Stadt','76726','07 3 34 007'],['Eisenberg (Pfalz), Stadt','67304','07 3 33 019'],
    ['Lambrecht (Pfalz), Stadt','67466','07 3 32 032'],['Wöllstein','55597','07 3 31 072'],
    ['Kell am See','54427','07 2 35 058'],['Prüm, Stadt','54595','07 2 32 296'],
    ['Westerburg, Stadt','56457','07 1 43 308'],['Selters (Westerwald), Stadt','56242','07 1 43 067'],
    ['Rennerod, Stadt','56477','07 1 43 286'],['Köln, Stadt','50667','05 3 15 000'],
    ['Flensburg, Stadt','24937','01 0 01 000 24937'],['Garding, Stadt','25836','01 0 54 036'],
    ['Reichenbach/O.L., Stadt','02894','14 6 26 450'],['Kargow','17192','13 0 71 071'],
    ['Föckelberg','66887',''],['Büddenstedt','38372','03 1 54 003'],['Bürgel, Stadt','07616','16 0 74 009']
]