# -*- coding: utf-8 -*-
import scrapy
from deland.items import CHItem


class ChSpider(scrapy.Spider):
    name = "ch"
    allowed_domains = ["en.wikipedia.org"]
    start_urls = ['http://en.wikipedia.org/']
    custom_settings = {'ITEM_PIPELINES': {'deland.pipelines_ch.DelandPipeline': 400}}

    def parse(self, response):
        #yield scrapy.Request('https://en.wikipedia.org/wiki/Cuarnens',callback=self.parse_municipality,meta={'obj':{}})
        #return
        items = self.pipeline.data['items']
        for obj in items:
            bfs_no = obj['e_bfs_no']
            while len(bfs_no) < 4:
                bfs_no = '0' + bfs_no
            self.pipeline.data['e_bfs_no'].append(bfs_no)
            link = 'https://en.wikipedia.org/w/index.php?fulltext=1&search=%s, Switzerland %s' % (obj['e_gemeindename'],bfs_no)
            yield scrapy.Request(link,callback=self.parse_search,meta={'obj':obj})

    def parse_search(self, response):
        links = response.xpath("//ul[contains(@class,'mw-search-results')]/li/div/a/@href").extract()
        if links:
            relative_url = links[0]
            link = response.urljoin(relative_url)
            obj = response.meta.get('obj')
            yield scrapy.Request(link,callback=self.parse_municipality,meta={'links':links[1:],'obj':obj,'search_url':response.request.url})
            
    def parse_municipality(self, response):
        obj = response.meta.get('obj')
        if (response.xpath("//th[text()='Country']/following-sibling::td/a[text()='Switzerland']") or 
            response.xpath("//a[text()='Country']/parent::th/following-sibling::td/a[text()='Switzerland']")):
            item = CHItem()
            for key in obj:
                item[key] = obj[key]
            item['w_title'] = response.xpath('//h1/text()').extract_first()
            item['w_url'] = response.request.url
            item['w_coat_of_arms'] = response.xpath("//small[text()='Coat of arms']/parent::td/a/img/@src").extract_first()
            item['w_canton'] = response.xpath("//a[text()='Canton']/parent::th/following-sibling::td/descendant-or-self::*/text()").extract_first()
            item['w_district'] = response.xpath("//a[text()='District']/parent::th/following-sibling::td/descendant-or-self::*/text()").extract_first()
            item['w_major'] = ' '.join(response.xpath("//a[text()='Mayor']/parent::th/following-sibling::td/descendant-or-self::*/text()").extract()).replace('\n',' ')
            item['w_area'] = (response.xpath("//th[contains(text(),'Area')]/parent::tr/following-sibling::tr/td/text()").extract_first() or '').split('km')[0].strip().replace('.',',')
            item['w_height'] = (response.xpath("//th[text()='Elevation']/following-sibling::td/text()").extract_first() or '').split('m')[0].strip()
            item['w_population'] = (response.xpath("//th[contains(text(),'Population')]/parent::tr/following-sibling::tr/td/text()").extract_first() or '').strip().replace(',','').replace('.',',')
            item['w_postal_codes'] = response.xpath("//a[text()='Postal code']/parent::th/following-sibling::td/span/text()").extract_first()
            item['w_website'] = response.xpath("//th[text()='Website']/following-sibling::td//a/@href").extract_first()
            item['w_sfos'] = response.xpath("//a[text()='SFOS number']/parent::th/following-sibling::td/text()").extract_first()
            yield item
        else:
            print('no data for %s' % response.meta.get('search_url'))
            if False:
                links = response.meta.get('links')
                if links:
                    relative_url = links[0]
                    link = response.urljoin(relative_url)
                    yield scrapy.Request(link,callback=self.parse_municipality,meta={'links':links[1:],'obj':obj})
                else:
                    print('no links!')
