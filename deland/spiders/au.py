# -*- coding: utf-8 -*-
import scrapy
from deland.items import AUItem
from deland import settings

class AuSpider(scrapy.Spider):
    name = "au"
    allowed_domains = ["de.wikipedia.org"]
    start_urls = ['https://de.wikipedia.org/wiki/NUTS:AT']
    custom_settings = {'ITEM_PIPELINES': {'deland.pipelines_au.DelandPipeline': 400}}

    def parse(self, response):
        #yield scrapy.Request('https://de.wikipedia.org/wiki/Traiskirchen',callback=self.parse_bezirke,meta={'obj':{}})
        #return
        trs = response.xpath("//table[@class='wikitable']/tr")[1:]
        code_1 = code_2 = code_3 = name_1 = name_2 = name_3 = ''
        for tr in trs:
            tds = tr.xpath('td')
            if len(tds)==4:
                code_1 = tds[0].xpath('text()').extract_first()
                name_1 = tds[0].xpath('a/text()').extract_first()
                if settings.ONLY_NUT:
                    obj = {'w_code_1':code_1,'w_name_1':name_1}
                    relative_url = tds[0].xpath('a/@href').extract_first()
                    link = response.urljoin(relative_url)
                    yield scrapy.Request(link,callback=self.parse_bezirke,meta={'obj':obj})
            if len(tds)>=3:
                code_2 = tds[-3].xpath('text()').extract_first()
                name_2 = tds[-3].xpath('a/text()').extract_first()
                if settings.ONLY_NUT:
                    obj = {'w_code_1':code_1,'w_name_1':name_1,'w_code_2':code_2,'w_name_2':name_2}
                    relative_url = tds[-3].xpath('a/@href').extract_first()
                    link = response.urljoin(relative_url)
                    yield scrapy.Request(link,callback=self.parse_bezirke,meta={'obj':obj})
            code_3 = tds[-2].xpath('text()').extract_first()
            name_3 = tds[-2].xpath('a/text()').extract_first()
            if settings.ONLY_NUT:
                    obj = {'w_code_1':code_1,'w_code_2':code_2,'w_code_3':code_3,
                        'w_name_1':name_1,'w_name_2':name_2,'w_name_3':name_3}
                    relative_url = tds[-2].xpath('a/@href').extract_first()
                    link = response.urljoin(relative_url)
                    yield scrapy.Request(link,callback=self.parse_bezirke,meta={'obj':obj})
                    continue
            a_elems = tds[-1].xpath('a')
            for a in a_elems:
                relative_url = a.xpath('@href').extract_first()
                title = a.xpath('text()').extract_first()
                obj = {'w_code_1':code_1,'w_code_2':code_2,'w_code_3':code_3,
                    'w_name_1':name_1,'w_name_2':name_2,'w_name_3':name_3,
                    'w_bezirke_name':title}
                link = response.urljoin(relative_url)
                yield scrapy.Request(link,callback=self.parse_bezirke,meta={'obj':obj})
            
    def parse_bezirke(self, response):
        obj = response.meta.get('obj')
        founded_bezirke = False
        if not settings.ONLY_NUT:
            trs = response.xpath("//table[@class='wikitable sortable']//tr")[1:]
            for tr in trs:
                relative_url = tr.xpath('td[1]/b/a/@href').extract_first()
                if relative_url:
                    link = response.urljoin(relative_url)
                    founded_bezirke = True
                    yield scrapy.Request(link,callback=self.parse_bezirke,meta={'obj':obj})
        if not founded_bezirke:
            w_lau2_nat_code = (response.xpath("//a[text()='Gemeindekennziffer']/parent::td/following-sibling::td/text()").extract_first() or '').strip().replace(' ','').replace(chr(160),'')
            if w_lau2_nat_code in self.pipeline.data['items'] or settings.ONLY_NUT:
                item = AUItem()
                for key in obj:
                    item[key] = obj[key]
                if w_lau2_nat_code in self.pipeline.data['items']:    
                    e_item = self.pipeline.data['items'][w_lau2_nat_code]
                    for key in e_item:
                        item[key] = e_item[key]
                item['w_title'] = response.xpath('//h1/text()').extract_first()
                item['w_url'] = response.request.url
                item['w_image_url'] = response.xpath("//th[contains(text(),'Wappen')]/parent::tr/following-sibling::tr//img/@src").extract_first()
                item['w_state'] = response.xpath("//a[text()='Politischer Bezirk']/parent::td/following-sibling::td/a/text()").extract_first()
                item['w_height'] = (response.xpath("//a[text()='Höhe']/parent::td/following-sibling::td/span/text()").extract_first() or '').strip()
                item['w_area'] = (response.xpath("//td[contains(text(),'Fläche:')]/following-sibling::td/text()").extract_first() or '').split('km')[0].strip()
                item['w_postal_codes'] = (response.xpath("//a[contains(text(),'Postleitzahl')]/parent::td/following-sibling::td/text()").extract_first() or '').strip()
                item['w_national_code'] = (response.xpath("//a[text()='Vorwahl']/parent::td/following-sibling::td/text()").extract_first() or '').strip()
                item['w_lau2_nat_code'] = (response.xpath("//a[text()='Gemeindekennziffer']/parent::td/following-sibling::td/text()").extract_first() or '').strip()
                item['w_administration_address'] = ' '.join(response.xpath("//td[contains(text(),'Adresse der')]/following-sibling::td/descendant-or-self::*/text()").extract() or []).strip().replace('\n','')
                item['w_website'] = response.xpath("//td[contains(text(),'Website:')]/following-sibling::td/span/a/text()").extract_first()
                item['w_major'] = ''.join(response.xpath("//a[contains(text(),'Bürgermeister')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract() or []).split('(')[0].strip()
                yield item
            else:
                pass#print(response.request.url)