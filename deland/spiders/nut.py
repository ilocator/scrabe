# -*- coding: utf-8 -*-
import scrapy
import xlrd
from deland.items import DelandItem
from deland import settings

class NutSpider(scrapy.Spider):
    name = "nut"
    allowed_domains = ["de.wikipedia.org"]
    start_urls = ['https://de.wikipedia.org/wiki/NUTS:DE']
    #custom_settings = {'ITEM_PIPELINES': {'deland.pipelines_nut.DelandPipeline': 400}}
    
    def parse(self, response):
        rows = response.xpath("//table[@class='wikitable toptextcells']/tr")
        for i in range(len(rows)):
            if i>0:
                row = rows[i]
                tds = row.xpath('td')
                raw_link = tds[-1].xpath('a/@href').extract_first()
                if raw_link:
                    link = response.urljoin(raw_link)
                    obj = {}
                    link_1 = link_2 = ''
                    obj['w_nut_3_code'] = tds[-2].xpath('text()').extract_first()
                    obj['w_nut_3_name'] = tds[-1].xpath('a/text()').extract_first()
                    if len(tds)>2:
                        code_2 = tds[-4].xpath('text()').extract_first()
                        name_2 = tds[-3].xpath('descendant-or-self::*/text()').extract_first()
                        raw_link_2 = tds[-3].xpath('a/@href').extract_first()
                        if raw_link_2:
                            link_2 = response.urljoin(raw_link_2)
                    obj['w_nut_2_code'] = code_2
                    obj['w_nut_2_name'] = name_2
                    if len(tds)>4:
                        code_1 = tds[0].xpath('text()').extract_first()
                        name_1 = tds[1].xpath('a/text()').extract_first()
                        raw_link_1 = tds[1].xpath('a/@href').extract_first()
                        if raw_link_1:
                            link_1 = response.urljoin(raw_link_1)
                    obj['w_nut_1_code'] = code_1
                    obj['w_nut_1_name'] = name_1
                    yield scrapy.Request(link,callback=self.parse_nut,meta={'obj':obj,'link_1':link_1,'link_2':link_2})
                else:
                    if len(tds)>4:
                        code_1 = tds[0].xpath('text()').extract_first()
                        name_1 = tds[1].xpath('a/text()').extract_first()
                        raw_link_1 = tds[1].xpath('a/@href').extract_first()
                        obj = {'w_nut_1_code':code_1,'w_nut_1_name':name_1}
                        if raw_link_1:
                            link_1 = response.urljoin(raw_link_1)
                            yield scrapy.Request(link_1,callback=self.parse_nut,meta={'obj':obj})
                        
    
    def parse_nut(self, response):
        obj = response.meta.get('obj')
        item = DelandItem()
        item['w_title'] = response.xpath('//h1/text()').extract_first()
        item['w_url'] = response.request.url
        item['w_image_url'] = response.xpath("//th[text()='Wappen']/parent::tr/following-sibling::tr/td[1]/a/img/@src").extract_first()
        item['w_state'] = response.xpath("//a[text()='Bundesland']/parent::td/following-sibling::td/a/text()").extract_first()
        item['w_height'] = (response.xpath("//a[text()='Höhe']/parent::td/following-sibling::td/text()").extract_first() or '').split("\u00a0")[0]
        item['w_area'] = (response.xpath("//a[text()='Fläche']/parent::td/following-sibling::td/text()").extract_first() or 
            response.xpath("//td[text()='Fläche:']/following-sibling::td/text()").extract_first() or '').split(' ')[0].split('km')[0].strip().replace('.','').replace(',','.')
        item['w_inhabitants'] = (response.xpath("//td[text()='Einwohner:']/following-sibling::td/text()").extract_first() or
            response.xpath("//a[text()='Einwohner']/parent::td/following-sibling::td/text()").extract_first() or '').replace('.','')
        item['w_postal_codes'] = response.xpath("//a[starts-with(text(),'Postleitzahlen')]/parent::td/following-sibling::td/text()").extract_first()
        item['w_area_code'] = response.xpath("//a[text()='Vorwahl' or text()='Vorwahlen']/parent::td/following-sibling::td/text()").extract_first()
        item['w_license_number'] = response.xpath("//a[text()='Kfz-Kennzeichen']/parent::td/following-sibling::td/text()").extract_first()
        national_code = response.meta.get('national_code')
        if not national_code:
            national_code = (response.xpath("//a[text()='Gemeindeschlüssel' or text()='Kreisschlüssel']/parent::td/following-sibling::td/text()").extract_first() or '')[:7]
        item['w_national_code'] = national_code
        item['w_city_breakdown'] = ' '.join(response.xpath("//td[text()='Stadtgliederung:']/following-sibling::td/text()").extract()).replace('\n','')
        item['w_administration_address'] = ' '.join(response.xpath("//td[contains(text(),'Adresse der')]/following-sibling::td/text()").extract()).replace('\n','')
        item['w_website'] = (response.xpath("//td[text()='Webpräsenz:' or text()='Website:']/following-sibling::td/a/text()").extract_first() or
            response.xpath("//a[text()='Webpräsenz' or text()='Website']/parent::td/following-sibling::td/a/text()").extract_first() or '')
        item['w_major'] = (response.xpath("//a[starts-with(text(),'Oberbürgermeister')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or
            response.xpath("//td[text()='Gemeinschaftsvorsitzender:']/following-sibling::td/descendant-or-self::*/text()").extract_first() or
            response.xpath("//a[starts-with(text(),'Bürgermeister')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or ''
            ).split('(')[0].strip()
        for key in obj:
            item[key] = obj[key]
        yield item
        link_1 = response.meta.get('link_1')
        if link_1:
            obj_1 = {'w_nut_1_code':obj['w_nut_1_code'],'w_nut_1_name':obj['w_nut_1_name']}
            yield scrapy.Request(link_1,callback=self.parse_nut,meta={'obj':obj_1,'national_code':national_code[:2]})
        link_2 = response.meta.get('link_2')
        if link_2:
            obj_2 = {'w_nut_1_code':obj['w_nut_1_code'],'w_nut_1_name':obj['w_nut_1_name'],'w_nut_2_code':obj['w_nut_2_code'],'w_nut_2_name':obj['w_nut_2_name']}
            yield scrapy.Request(link_2,callback=self.parse_nut,meta={'obj':obj_2,'national_code':national_code[:4]})    
            
