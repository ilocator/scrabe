# -*- coding: utf-8 -*-
import scrapy
from deland.items import NLItem

class NlSpider(scrapy.Spider):
    name = "nl"
    allowed_domains = ["de.wikipedia.org","en.wikipedia.org"]
    start_urls = ['https://de.wikipedia.org/wiki/NUTS:NL']
    custom_settings = {'ITEM_PIPELINES': {'deland.pipelines_nl.DelandPipeline': 400}}

    def parse(self, response):
        trs = response.xpath("//table[@class='wikitable']/tr")[2:]
        code_1 = code_2 = name_1 = name_2 = ''
        provincie = {}
        for tr in trs:
            tds = tr.xpath('td')
            name_3 = tds[-1].xpath('text()').extract_first() or tds[-1].xpath('a/text()').extract_first()
            code_3 = tds[-2].xpath('text()').extract_first()
            self.pipeline.data['nut_3'][code_3] = name_3
            if len(tds)==6:
                code_1 = tds[0].xpath('text()').extract_first()
                name_1 = tds[1].xpath('text()').extract_first().strip()
            if len(tds)>=4:
                code_2 = tds[-4].xpath('text()').extract_first()
                name_2 = tds[-3].xpath('a[last()]/text()').extract_first()
                relative_url = tds[-3].xpath('a[last()]/@href').extract_first()
                obj = {'w_code_1':code_1,'w_code_2':code_2,'w_name_1':name_1,'w_name_2':name_2}
                provincie[name_2] = obj
                url = response.urljoin(relative_url)
                yield scrapy.Request(url,callback=self.parse_province,meta={'obj':obj})
        link = 'https://en.wikipedia.org/wiki/List_of_municipalities_of_the_Netherlands'
        yield scrapy.Request(link,callback=self.parse_municipalities,meta={'provincie':provincie})

    def parse_province(self, response):
        item = NLItem()
        obj = response.meta.get('obj')
        for key in obj:
            item[key] = obj[key]
        item['w_url'] = response.url
        item['w_title'] = response.xpath('//h1/text()').extract_first()
        item['w_coat_of_arms'],item['w_flag'] = response.xpath("//small[text()='Wappen']/parent::td/parent::tr/following-sibling::tr/td/a/img/@src").extract()
        item['w_major'] = response.xpath("//a[text()='Königlicher Kommissar']/parent::td/following-sibling::td/a/text()").extract_first()
        item['w_website'] = response.xpath("//td[contains(text(),'Website')]/following-sibling::td/a/text()").extract_first()
        item['w_population'] = response.xpath("//a[text()='Einwohner']/parent::td/following-sibling::td/text()").extract_first().replace('.','').strip()
        item['w_area'] = response.xpath("//a[text()='Fläche']/parent::td/following-sibling::td/text()").extract_first().replace('.','').split('km')[0].strip()
        yield item
        
    def parse_municipalities(self, response):
        trs = response.xpath("//table[contains(@class,'wikitable')][1]/tr")[1:]
        provincie = response.meta.get('provincie')
        for tr in trs:
            tds = tr.xpath('td')
            relative_url = tr.xpath('th//a/@href').extract_first()
            if relative_url:
                obj = {}
                obj['w_url'] = response.urljoin(relative_url)
                obj['w_cbs_code'] = tds[0].xpath('text()').extract_first()
                #obj['w_population'] = tds[2].xpath('text()').extract_first().strip().replace(',','')
                #obj['w_area'] = (tds[4].xpath('text()').extract_first() or '').split('km')[0].strip()
                raw_provincie_key = tds[1].xpath('a/text()').extract_first()
                provincie_exceptions = {'North Brabant':'Noord-Brabant','North Holland':'Noord-Holland',
                    'South Holland':'Zuid-Holland','Limburg':'Limburg (NL)','Friesland':'Friesland (NL)'}
                provincie_key = provincie_exceptions[raw_provincie_key] if raw_provincie_key in provincie_exceptions else raw_provincie_key
                obj.update(provincie[provincie_key])
                link = obj['w_url']
                yield scrapy.Request(link,callback=self.parse_municipality,meta={'obj':obj})
                
    def parse_municipality(self, response):
        obj = response.meta.get('obj')
        title = response.xpath('//h1/text()').extract_first()
        obj['w_title'] = title
        raw_flag = response.xpath("//small[text()='Flag']/parent::td/a/img/@src").extract_first()
        obj['w_flag'] = response.urljoin(raw_flag) if raw_flag else ''
        raw_coat = response.xpath("//small[text()='Coat of arms']/parent::td/a/img/@src").extract_first()
        obj['w_coat_of_arms'] = response.urljoin(raw_coat) if raw_coat else ''
        obj['w_major'] = (response.xpath("//a[text()='Mayor']/parent::th/parent::tr/td/a/text()").extract_first() or 
            response.xpath("//th[contains(text(),'Mayor')]/following-sibling::td/a/text()").extract_first() or '')
        obj['w_province'] = (response.xpath("//a[text()='Province']/parent::th/parent::tr/td/a/text()").extract_first() or
            response.xpath("//th[text()='Province']/following-sibling::td/a/text()").extract_first() or '')
        obj['w_postcode'] = (response.xpath("//a[text()='Postcode']/parent::th/parent::tr/td/span/text()").extract_first() or '')
        obj['w_areacode'] = (response.xpath("//a[text()='Area code']/parent::th/parent::tr/td/text()").extract_first() or '')
        obj['w_website'] = (response.xpath("//th[text()='Website']/parent::tr/td/span/a/@href").extract_first() or '')
        obj['w_population'] = (response.xpath("//th[contains(text(),'Population')]/parent::tr/following-sibling::tr/td/text()").extract_first() or '').replace(',','').strip()
        obj['w_area'] = (response.xpath("//th[contains(text(),'Area')]/parent::tr/following-sibling::tr/td/text()").extract_first() or '').replace(',','').split('km')[0].strip().replace('.',',')
        item = NLItem()
        for key in obj:
            item[key] = obj[key]
        excel_data = self.pipeline.data['items']
        if title in excel_data:
            for key in excel_data[title]:
                item[key] = excel_data[title][key]
        else:
            print("not find title: %s" % title)
        if 'e_nut_3_code' in item and item['e_nut_3_code'] in self.pipeline.data['nut_3']:
            item['w_name_3'] = self.pipeline.data['nut_3'][item['e_nut_3_code']]
        yield item    