# -*- coding: utf-8 -*-
import scrapy
import xlrd
from deland.items import DelandItem
from deland import settings

class LauSpider(scrapy.Spider):
    name = "lau"
    allowed_domains = ["de.wikipedia.org"]
    start_urls = ['http://de.wikipedia.org/']
    custom_settings = {'ITEM_PIPELINES': {'deland.pipelines_lau.DelandPipeline': 400}}

    def parse(self, response):
        items = self.pipeline.data['items']
        i = 0
        for key in items:
            i+= 1
            title = items[key]['e_gemeindename']
            vb = items[key]['e_vb']
            link = 'https://de.wikipedia.org/w/index.php?fulltext=1&search=%s %s' % (title,vb)
            self.pipeline.data['input'].append(key)
            yield scrapy.Request(link,callback=self.parse_search,meta={'i':key})
            
    def parse_search(self, response):
        links = response.xpath("//ul[contains(@class,'mw-search-results')]/li/div/a/@href").extract()
        if links:
            relative_url = links[0]
            link = response.urljoin(relative_url)
            yield scrapy.Request(link,callback=self.parse_land,dont_filter=True,meta={'links':links[1:],'i':response.meta.get('i')})
    
    def parse_land(self, response):
        i = response.meta.get('i')
        if response.xpath("//th[text()='Basisdaten']") and response.xpath("//th[text()='Wappen']") and response.xpath("//a[text()='Amtsschlüssel' or text()='Verbandsschlüssel']"): 
            item = DelandItem()
            item['w_url'] = response.request.url
            item['w_title'] = response.xpath('//h1/text()').extract_first()
            item['w_national_code'] = response.xpath("//a[text()='Amtsschlüssel' or text()='Verbandsschlüssel']/parent::td/following-sibling::td/text()").extract_first()
            item['w_image_url'] = response.xpath("//th[text()='Wappen']/parent::tr/following-sibling::tr/td[1]/a/img/@src").extract_first()
            item['w_city_breakdown'] = ' '.join(response.xpath("//td[text()='Stadtgliederung:']/following-sibling::td/text()").extract()).replace('\n','')
            item['w_height'] = (response.xpath("//a[text()='Höhe']/parent::td/following-sibling::td/text()").extract_first() or '').split("\u00a0")[0]
            item['w_area'] = (response.xpath("//a[text()='Fläche']/parent::td/following-sibling::td/text()").extract_first() or '').split("\u00a0")[0].replace('.',',')
            item['w_license_number'] = response.xpath("//a[text()='Kfz-Kennzeichen']/parent::td/following-sibling::td/text()").extract_first()
            item['w_inhabitants'] = response.xpath("//td[text()='Einwohner:']/following-sibling::td/text()").extract_first()
            item['w_administration_address'] = ' '.join(response.xpath("//td[contains(text(),'Adresse der')]/following-sibling::td/text()").extract()).replace('\n','')
            item['w_website'] = response.xpath("//td[text()='Webpräsenz:']/following-sibling::td/a/text()").extract_first()
            item['w_state'] = response.xpath("//a[text()='Bundesland']/parent::td/following-sibling::td/a/text()").extract_first()
            item['w_postal_codes'] = response.xpath("//a[starts-with(text(),'Postleitzahl')]/parent::td/following-sibling::td/text()").extract_first()
            item['w_major'] = (response.xpath("//a[starts-with(text(),'Oberbürgermeister')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or
                response.xpath("//a[starts-with(text(),'Bürgermeister')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or 
                response.xpath("//a[starts-with(text(),'Vorsitzende')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or 
                response.xpath("//a[starts-with(text(),'Samtgemeinde-')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or 
                response.xpath("//a[starts-with(text(),'Amtsdirektor')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or 
                response.xpath("//a[starts-with(text(),'Amtsvorsteher')]/parent::td/following-sibling::td/descendant-or-self::*/text()").extract_first() or 
                response.xpath("//td[text()='Verbandsvorsitzender:' or text()='Verbandsgemeinde- bürgermeister:' or text()='Verwaltungsvorsitzender:' or text()='Verwaltungsvorsitzende:']/following-sibling::td/descendant-or-self::*/text()").extract_first() or ''
                ).split('(')[0].strip()
                

            for key in self.pipeline.data['items'][i]:
                item[key] = self.pipeline.data['items'][i][key]
            self.pipeline.data['output'].append(i)    
            yield item
        else:
            links = response.meta.get('links')
            if links:
                relative_url = links[0]
                link = response.urljoin(relative_url)
                yield scrapy.Request(link,callback=self.parse_land,dont_filter=True,meta={'links':links[1:],'i':i})
                        