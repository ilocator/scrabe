# -*- coding: utf-8 -*-
import scrapy
from deland.items import UKItem

def process_td(td,response,pipeline,items_keys,code_1,code_2,code_3):
    links = td.xpath('a')
    result = []
    for raw_link in links:
        relative_link = raw_link.xpath('@href').extract_first()
        link_text = raw_link.xpath('text()').extract_first()
        link = response.urljoin(relative_link)
        if link_text in pipeline.data['items'].keys():
            if link_text in items_keys:
                items_keys.remove(link_text)
            obj = pipeline.data['items'][link_text]
            obj.update({'w_code_1':code_1,'w_code_2':code_2,'w_code_3':code_3})
            result.append([link,obj])
    return result

class UkSpider(scrapy.Spider):
    name = "uk"
    allowed_domains = ["en.wikipedia.org"]
    start_urls = ['https://en.wikipedia.org/wiki/NUTS_statistical_regions_of_the_United_Kingdom']
    custom_settings = {'ITEM_PIPELINES': {'deland.pipelines_uk.DelandPipeline': 400}}

    def parse(self, response):
        trs = response.xpath("//table[@class='wikitable'][2]/tr")
        items_keys = list(self.pipeline.data['items'].keys())
        print('start items: %s' % len(items_keys))
        code_1 = code_2 = code_3 = ''
        for tr in trs[1:-1]:
            tds = tr.xpath('td')
            if len(tds)==6:
                code_1 = tds[1].xpath('text()').extract_first()
                for link,obj in process_td(tds[0],response,self.pipeline,items_keys,code_1,code_2='',code_3=''):
                    yield scrapy.Request(link,callback=self.parse_land,meta={'obj':obj})
            if len(tds)>=4:
                code_2 = tds[3].xpath('text()').extract_first()
                for link,obj in process_td(tds[2],response,self.pipeline,items_keys,code_1,code_2,code_3=''):
                    yield scrapy.Request(link,callback=self.parse_land,meta={'obj':obj})
            code_3 = tds[-1].xpath('text()').extract_first()
            for link,obj in process_td(tds[-2],response,self.pipeline,items_keys,code_1,code_2,code_3):
                yield scrapy.Request(link,callback=self.parse_land,meta={'obj':obj})
        for key in items_keys:
            link = self.pipeline.data['items'][key]['e_url']
            if 'http' in link:
                obj = self.pipeline.data['items'][key]
                yield scrapy.Request(link,callback=self.parse_land,meta={'obj':obj})
    
    def parse_land(self, response):
        obj = response.meta.get('obj')
        obj['w_coat_of_arms'] = (response.xpath("//img[contains(@alt,'Arms') or contains(@alt,'arms') or contains(@alt,'Official logo') or contains(@alt,'Flag of')]/@src").extract_first() or
            response.xpath("//small[@text='Coat of arms']/parent::td/a").extract_first()
        )
        obj['w_population'] = (response.xpath("//th[contains(text(),'Population')]/parent::tr/following-sibling::tr/th[contains(text(),'Total')]/following-sibling::td/text()").extract_first() or
            response.xpath("//th[contains(text(),'Population')]/following-sibling::td/text()").extract_first() or
            '').replace(',','')
        obj['w_area'] = (
            (response.xpath("//th[contains(text(),'Area')]/parent::tr/following-sibling::tr/th[contains(text(),'Total')]/following-sibling::td/text()").extract_first() or '').split('km')[0].split('(')[-1] or
            (response.xpath("//th[contains(text(),'Area')]/following-sibling::td/text()").extract_first() or '').split('km')[0]
        ).strip().replace(',','').replace('.',',')
        item = UKItem()
        item.update(obj)
        yield item