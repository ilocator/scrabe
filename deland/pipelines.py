# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from deland import settings,lib

def get_nuts(raw):
    result = {}
    nut_1_code = nut_1_name = nut_2_code = nut_2_name = ''
    for i in range(len(raw)):
        if i>settings.EXCEL_SPACES['nuts']:
            row = raw[i]
            nut_1_code = row[0] or nut_1_code
            nut_1_name = row[1] or nut_1_name
            nut_2_code = row[2] or nut_2_code
            nut_2_name = row[3] or nut_2_name
            nut_3_code = row[4]
            nut_3_name = row[5]
            result[nut_3_code] = {'e_nut_1_code':nut_1_code,'e_nut_1_name':nut_1_name,'e_nut_2_code':nut_2_code,
                'e_nut_2_name':nut_2_name,'e_nut_3_code':nut_3_code,'e_nut_3_name':nut_3_name}
    return result

def get_lau(raw):
    result = {}
    fields = ['nuts_3','lau1_nat_code','lau2_nat_code','change','name_1','name_2_lat','pop','area']
    for i in range(len(raw)):
        #zero_pop = False #REMOVE!
        if i>settings.EXCEL_SPACES['lau']:
            row = raw[i]
            obj = {}
            for j in range(len(fields)):
                field_name = 'e_%s' % fields[j]
                if fields[j]=='pop':
                    obj[field_name] = row[j].split('.')[0]
                    #if row[j].split('.')[0]=='0': #REMOVE!
                    #    zero_pop = True #REMOVE!
                elif fields[j]=='area':
                    obj[field_name] = row[j].split('.')[0]
                else:
                    obj[field_name] = row[j]
            #obj['lau_i'] = i+1 #REMOVE!
            #if zero_pop: continue #REMOVE!
            result[row[4]+row[6].split('.')[0]] = obj
            result[row[4]] = obj
    return result

def get_gemeinden(raw):
    result = []
    fields = ['land','rb','kreis','vb','gem','gemeindename','km2','einwohner','einwohner_km2','plz','langengrad','breitengrad']
    for i in range(len(raw)):
        if i>settings.EXCEL_SPACES['gemeinden']:
            row = raw[i]
            if row[9]!='':
                obj = {}
                for j in range(len(fields)):
                    field_name = 'e_%s' % fields[j]
                    if fields[j] in ['einwohner_km2','einwohner']:
                        obj[field_name] = row[j].split('.')[0]
                    elif fields[j]=='km2':
                        obj[field_name] = row[j].replace('.',',')
                    else:    
                        obj[field_name] = row[j]
                obj['e_national_code'] = ' '.join([obj['e_%s' % f] for f in ['land','rb','kreis','gem']])
                obj['i'] = i+1    
                result.append(obj)
    return result
        
class DelandPipeline(object):
    def open_spider(self, spider):
        excel = lib.get_excel()
        nuts = get_nuts(excel[2])
        lau = get_lau(excel[1])
        #lau_taken_list = [] #REMOVE!
        gemeinden = get_gemeinden(excel[0])
        print('!!!%s' % len (gemeinden))
        items = {}
        errors = []
        for obj in gemeinden:
            new_obj = {'completed':1}
            new_obj.update(obj)
            lau_key = obj['e_gemeindename']+obj['e_einwohner']
            if lau_key in lau:
                lau_obj = lau[lau_key]
            elif obj['e_gemeindename'] in lau:
                lau_obj = lau[obj['e_gemeindename']]
            else:
                errors.append([obj['i'],'Missed lau key'])
                new_obj['completed'] = 0
                items[obj['i']] = new_obj #Wiki search without second page
                continue
            new_obj.update(lau_obj)
            #lau_taken_list.append(lau_obj['lau_i']) #REMOVE!
            nut_key = new_obj['e_nuts_3']
            if nut_key not in nuts:
                errors.append([obj['i'],'Missed nut key'])
                new_obj['completed'] = 0
                items[obj['i']] = new_obj #Wiki search without third page
                continue
            nut_obj = nuts[nut_key]
            new_obj.update(nut_obj)
            items[obj['i']] = new_obj
        #lau_total_list = [] #REMOVE!
        #for key in lau: #REMOVE!
        #    lau_total_list.append(lau[key]['lau_i']) #REMOVE!
        #open('missed_lau.txt','w').write('') #REMOVE!
        #for i in sorted(list( set(lau_total_list) - set(lau_taken_list))): #REMOVE!
        #    open('missed_lau.txt','a').write(str(i)+'\n') #REMOVE!
        self.data = {'items':items,'errors':errors,'input':[],'output':[]}
        spider.pipeline = self

    def close_spider(self, spider):
        missed_file = 'wiki_missed.txt'
        open(missed_file,'w').write('')
        for value in sorted(list(set(self.data['input'])-set(self.data['output']))):
            open(missed_file,'a').write(str(value)+'\n')
        merge_fail_file = 'merge_fail.txt'
        open(merge_fail_file,'w').write('')
        for i,text in self.data['errors']:
            open(merge_fail_file,'a').write('%s\t%s\n' % (i,text))

    def process_item(self, item, spider):
        return item