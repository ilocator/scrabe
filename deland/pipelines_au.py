# -*- coding: utf-8 -*-
from deland import lib
from deland import settings

def get_data(raw):
    result = {}
    for i in range(len(raw)):
        if i > settings.EXCEL_SPACES['au']:
            row = raw[i]
            obj = {}
            obj['e_nut_3_code'] = row[0]
            e_lau2_nat_code = row[2].split('.')[0]
            obj['e_lau2_nat_code'] = row[2].replace('.','')
            obj['e_title'] = row[4]
            obj['e_pop'] = row[6].split('.')[0]
            obj['e_area'] = row[7].split('.')[0]
            result[e_lau2_nat_code] = obj
    return result

class DelandPipeline(object):
    def open_spider(self, spider):
        excel = lib.get_excel(settings.EXCEL_AU)[0]
        data = get_data(excel)
        
        self.data = {'items':data}
        spider.pipeline = self
        
    def process_item(self, item, spider):
        return item

    def close_spider(self, spider):
        pass